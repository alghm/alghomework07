﻿using System.Diagnostics;

var array_1_1000 = new int[1000];
var array_2_1000 = new int[1000];
var array_1_10_000 = new int[10_000];
var array_2_10_000 = new int[10_000];
var array_1_100_000 = new int[100_000];
var array_2_100_000 = new int[100_000];
var array_1_1000_000 = new int[1000_000];
var array_2_1000_000 = new int[1000_000];
var array_2_10_000_000 = new int[10_000_000];
var array_2_100_000_000 = new int[100_000_000];

for (int i = 0; i < 1000; i++)
{
    var number = new Random().Next(0, 1000);
    array_1_1000[i] = number;
    array_2_1000[i] = number;
}


for (int i = 0; i < 10_000; i++)
{
    var number = new Random().Next(0, 10_000);
    array_1_10_000[i] = number;
    array_2_10_000[i] = number;
}

for (int i = 0; i < 100_000; i++)
{
    var number = new Random().Next(0, 100_000);
    array_1_100_000[i] = number;
    array_2_100_000[i] = number;
}

for (int i = 0; i < 1000_000; i++)
{
    var number = new Random().Next(0, 1000_000);
    array_1_1000_000[i] = number;
    array_2_1000_000[i] = number;
}

for (int i = 0; i < 10_000_000; i++)
{
    var number = new Random().Next(0, 10_000_000);
    array_2_10_000_000[i] = number;
}

for (int i = 0; i < 100_000_000; i++)
{
    var number = new Random().Next(0, 100_000_000);
    array_2_100_000_000[i] = number;
}

Stopwatch stopwatch = new Stopwatch();

stopwatch.Start();
SelectionSort(array_1_1000);
stopwatch.Stop();
long elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("SelectionSort для 1000 - {0}", elapsedTime);

stopwatch.Restart();
SelectionSort(array_1_10_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("SelectionSort для 10 000 - {0}", elapsedTime);

stopwatch.Restart();
SelectionSort(array_1_100_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("SelectionSort для 100 000 - {0}", elapsedTime);

/*stopwatch.Restart();
SelectionSort(array_1_1000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("SelectionSort для 1 000 000 - {0}", elapsedTime);*/


stopwatch.Restart();
HeapSort(array_2_1000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("HeapSort для 1000 - {0}", elapsedTime);

stopwatch.Restart();
HeapSort(array_2_10_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("HeapSort для 10 000 - {0}", elapsedTime);

stopwatch.Restart();
HeapSort(array_2_100_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("HeapSort для 100 000 - {0}", elapsedTime);

stopwatch.Restart();
HeapSort(array_2_1000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("HeapSort для 1 000 000 - {0}", elapsedTime);

stopwatch.Restart();
HeapSort(array_2_10_000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("HeapSort для 10 000 000 - {0}", elapsedTime);

stopwatch.Restart();
HeapSort(array_2_100_000_000);
stopwatch.Stop();
elapsedTime = stopwatch.ElapsedMilliseconds;
Console.WriteLine("HeapSort для 100 000 000 - {0}", elapsedTime);

Console.ReadKey();


void SelectionSort(int[] array)
{
    int n = array.Length;

    for (int i = 0; i < n - 1; i++)
    {
        int minIndex = i;
        for (int j = i + 1; j < n; j++)
        {
            if (array[j] < array[minIndex])
            {
                minIndex = j;
            }
        }

        int temp = array[minIndex];
        array[minIndex] = array[i];
        array[i] = temp;
    }
}

void HeapSort(int[] array)
{
    int n = array.Length;

    for (int i = n / 2 - 1; i >= 0; i--)
        Heapify(array, n, i);

    for (int i = n - 1; i >= 0; i--)
    {
        int temp = array[0];
        array[0] = array[i];
        array[i] = temp;

        Heapify(array, i, 0);
    }
}

void Heapify(int[] array, int n, int i)
{
    int largest = i;  
    int left = 2 * i + 1;  
    int right = 2 * i + 2;

    if (left < n && array[left] > array[largest])
        largest = left;

    if (right < n && array[right] > array[largest])
        largest = right;

    if (largest != i)
    {
        int swap = array[i];
        array[i] = array[largest];
        array[largest] = swap;

        Heapify(array, n, largest);
    }
}